import React, { Component } from 'react';
import PropTypes from 'prop-types';

import VoicePlayer from './lib/VoicePlayer.js';
import VoiceRecognition from './lib/VoiceRecognition.js';
import BotCard from './BotCard';
import LoadingSvg from '../Auth/LoadingSvg';
import ChatBotSvg from './ChatBotSvg';

class VoicePlayerDemo extends Component {
 
  constructor(props) {
    super(props);
    this._ttsTextChange = this._ttsTextChange.bind(this);
    this._ttsEnd = this._ttsEnd.bind(this);
    this._ttsStart = this._ttsStart.bind(this);

    this._sttEnd = this._sttEnd.bind(this);
    this._sttResult = this._sttResult.bind(this);
    this._sttStart = this._sttStart.bind(this);

    this.state = {
      ttsPlay: false,
      ttsPause: false,
      ttsText:
        'Hello, How can I Help you',
      sttStart: false,
      sttStop: false,
    };
  }

  componentWillMount() {
    console.log(this.props);
   // this.props.getPatient('Sameena Sabungar');
  }

  _ttsTextChange = event => {
    const ttsText = event;
    console.log('Text changed event',event);
    this.setState({ ttsText }, () => {
      console.log('Text Chagne ==> ttsTextChange');
    });
  };

  _ttsEnd = () => {
    this.setState({ ttsPlay: false }, () => {
      this._sttStart();
      console.log('End ==> ttsEnd');
    });
  };

  _ttsStart(e) {
    this.setState({ ttsPlay: true, sttStart: false }, () => {
      console.log('Start ==> ttsStarted');
      console.log("the details",e);
    });
  }

  _sttEnd = () => { 
    this.setState({ sttStart: false, sttStop: false }, () => {
      console.log('End ==> sttEnd');
    });
  };

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
  }

  _sttResult = ({ finalTranscript }) => {
    const result = finalTranscript;
    this.setState({ sttStart: false }, () => {
      console.log('Speech to Text Result ==> sttResult');
      console.log("result here",result);

      if (result.indexOf('queries') > 1 || result.indexOf('query') > 1|| result.indexOf('doubt') > 1) {
      //  this.props.showPatientModal();
      console.log('yes please');
      this._ttsTextChange('yes please');
      this.setState({ ttsPlay: true });

      } 
      else if (result.indexOf('limitation') > 1 || result.indexOf('reserve') > 1|| result.indexOf('number') > 1) {
        //  this.props.showPatientModal();
        console.log('No, but will soon have limitation');
        this._ttsTextChange('No, but will soon have limitation');
        this.setState({ ttsPlay: true });
  
        }
       else if (result.indexOf('multiple') > 1 || result.indexOf('one') > 1|| result.indexOf('time') > 1) {
          //  this.props.showPatientModal();
          console.log('yes');
          this._ttsTextChange('yes you can');
          this.setState({ ttsPlay: true });
    
        }
      else if (result.indexOf('options') > 1 || result.indexOf('alternatives') > 1|| result.indexOf('time') > 1) {
            //  this.props.showPatientModal();
            console.log('please visit  product link options');
            this._ttsTextChange('please visit product link options');
            this.setState({ ttsPlay: true });
      
        }
        else if (result.indexOf('close') > 1 || result.indexOf('deposit') > 1|| result.indexOf('duration') > 1) {
              //  this.props.showPatientModal();
              console.log('yes, but minimum period should be 3 months');
              this._ttsTextChange('yes, but minimum period should be 3 months');
              this.setState({ ttsPlay: true });
        
        }
       else  if (result.indexOf('thanks') >= 0 || result.indexOf('thank') >= 0 ) {
                //  this.props.showPatientModal();
                console.log('you are welcome! Have a nice day!');
                this._ttsTextChange('you are welcome! Have a nice day!');
                this.setState({ ttsPlay: true });
          
        }
      else {
        console.log('gotToPhi');
        console.log('sorry i dont understand what you are saying');
        this._ttsTextChange('sorry i dont understand what you are saying');
        this.setState({ ttsPlay: true });
      }
    });
  };

  _sttStart() {
    this.setState({ sttStart: true }, () => {
      console.log('Speech to text Started ==> sttStart');
    });
  }

  render() {
    console.log("in voice player");
    let button = (
      <button className="chatbot-btn default" onClick={this._ttsStart}>
        <ChatBotSvg />
      </button>
    );

    if (this.state.ttsPlay) {
      button = (
        <button className="chatbot-btn speaking" onClick={this._ttsEnd}>
          <LoadingSvg />
          <span>Speaking...</span>
        </button>
      );
    }

    if (this.state.sttStart) {
      button = (
        <button className="chatbot-btn listening" onClick={this._sttEnd}>
          <LoadingSvg />
          <span>Listening...</span>
        </button>
      );
    }

    return (
      <div>
        <div className="chatbot">
          {button}
          {this.state.ttsPlay && <BotCard audioText={this.state.ttsText} />}
        </div>
        {this.state.ttsPlay &&
          <VoicePlayer
            play={this._ttsStart}
            pause={this.state.ttsPause}
            text={this.state.ttsText}
            onStart={this._ttsStart}
            onEnd={this._ttsEnd}
          />}
        {this.state.sttStart &&
          <VoiceRecognition
            onStart={this._sttStart}
            onEnd={this._sttEnd}
            onResult={this._sttResult}
            continuous={true}
            lang="en-US"
            stop={this.state.sttStop}
          />}
      </div>
    );
  }
}

VoicePlayerDemo.propTypes = {
  action: PropTypes.func,
  getPatient: PropTypes.func,
  getPhiDetail: PropTypes.func,
  showPatientModal: PropTypes.func,
};

export default VoicePlayerDemo;
