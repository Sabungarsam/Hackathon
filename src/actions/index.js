import { actionTypes as types, urls } from '../constants';
import { post } from '../helpers';
import _ from 'lodash';
import axios from 'axios';
import React from 'react';
export const logout = () => dispatch => {
  dispatch({ type: types.USER_LOGOUT });
};

export const signup = ({ email, password }) => dispatch => {
  dispatch({ type: types.SIGNUP_REQUEST });
  post({
    url: urls.SIGNUP,
    body: { email, password },
    success: types.SIGNUP_SUCCESS,
    failure: types.SIGNUP_FAILURE,
    dispatch,
  });
};

export const login = ({ email, password }) => dispatch => {
  console.log("in login ");
  dispatch({ type: types.LOGIN_REQUEST });
  post({
    url: urls.LOGIN,
    body: { email, password },
    success: types.LOGIN_SUCCESS,
    failure: types.LOGIN_FAILURE,
    dispatch,
  });
};

export const loginWithToken = () => (dispatch, getState) => {
  console.log("in login with token");
  const token = getState().user.token;
  console.log("token here",token);
  if (!token) {
    console.log("!token here",token);
    dispatch({ type: types.LOGIN_REQUEST });
    return;
  }

  axios.post('https://hackthonfunction.azurewebsites.net/api/PersonDetail?code=m8bz7hSfwXOyE3epgBnVEpDwFV5d8WwiQFoJ0Bhp14uRwKpEysbQWQ==', {'personId': token})
  .then(personResponse => {
    const data = {
      'person': personResponse.data,
      'token': personResponse.data.personId,
      'name': personResponse.data.name,
      'detail': personResponse.data.userData,
      'image': getState().user.webcam.image,
    };
    dispatch({ type: types.WEBCAM_SUCCESS, data});
  })
  .catch(personError => {
    console.log(personError);
    console.log("image url",fileData.imageURL);
    const data = personError;
    dispatch({ type: types.WEBCAM_ERROR, data});
  }); 
};
const verifydata = 
{
  "largePersonGroupId": "sameena_large_group1",
  "personId": "d0b5b435-0c7d-4342-ba5e-fe784c8db670"
};


export const uploadAzure = (file, imageblob) => dispatch => {
  const data = {
    'token': true,
    //'loading' : true,
  };
  let faceId;
 // dispatch({ type: types.WEBCAM_SUCCESS, data});
 // dispatch({ type: types.WEBCAM_REQUEST });
  
 

  axios.defaults.headers.post['Content-Type'] = 'application/json';
  let dataurl = _.split(file, ',');
  let fileData = {
    'imageURL': dataurl[1],
    
  };
  function blobToUint8Array(b) {
    var uri = URL.createObjectURL(b),
        xhr = new XMLHttpRequest(),
        i,
        ui8;

    xhr.open('GET', uri, false);
    xhr.send();

    URL.revokeObjectURL(uri);

    ui8 = new Uint8Array(xhr.response.length);

    for (i = 0; i < xhr.response.length; ++i) {
        ui8[i] = xhr.response.charCodeAt(i);
    }

    return ui8;
}
 
  var b = new Blob([imageblob], {type: 'application/octet-stream'});
  blobToUint8Array(b); 
  console.log("binary ",blobToUint8Array(b));

    axios.post(
        "https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true" , imageblob,
       
       {  headers: 
        {
          'content-type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key':'e6f521c66fea4942bd1dd77f80a0d21a' 
      } 
    }
  
    )
    .then(function(success) {
      console.log("in face api call");
      verifydata.faceId = success.data[0].faceId;

  
       verify(verifydata,data);
    })
    .catch(function() {
        console.log("error in detect");
    });

    /*
    axios.post(
      "https://westeurope.api.cognitive.microsoft.com/face/v1.0/persongroups/samyyy123_/persons/d0b5b435-0c7d-4342-ba5e-fe784c8db670/persistedFaces" , imageblob,
     
     {  headers: 
      {
        'content-type': 'application/octet-stream',
      'Ocp-Apim-Subscription-Key':'e6f521c66fea4942bd1dd77f80a0d21a' 
    } 
  }

  )
  .then(function(data) {
      console.log("success in adding faces to APi call",data);
  })
  .catch(function() {
      console.log("error");
  });
*/


 function verify(data,objectnew){

  if(data.faceId)
  {
    data= objectnew;
    console.log("the face id present here",data);
    dispatch({ type: types.WEBCAM_SUCCESS, data});

  }
 // console.log("in verify function call verify data after setting",verifydata);
    axios.post(
    "https://westeurope.api.cognitive.microsoft.com/face/v1.0/verify" ,verifydata,
   
   {  headers: 
    {
      'content-type': 'application/json',
    'Ocp-Apim-Subscription-Key':'e6f521c66fea4942bd1dd77f80a0d21a' 
  } 
}

)
.then(function(data) {
    console.log("success in verify",data);
})
.catch(function(data) {
    console.log("error in verify",data);
});
 }

/*
  axios.post('https://westeurope.api.cognitive.microsoft.com/face/v1.0', imageblob)
  .then(response => {
    let faceObj = {
      'url': response.data.Message,
    };
*/
 // console.log("url of image",fileData.imageURL);
 // console.log("url of image",JSON.stringify(fileData));
 // console.log("image url in upload azure heelooo",fileData.imageURL);
  /* axios.post('//hackthonapp.azurewebsites.net/api/Upload/user/PostUserImage', JSON.stringify(fileData))
  .then(response => {
    let faceObj = {
      'url': response.data.Message,
    };
   
 
    axios.post('https://hackthonfunction.azurewebsites.net/api/HttpTriggerJS1?code=RfbEiaecqduava7lOVX0LNxPQrRVJjXIpoH8MB3J9iaI6wFm5IXQJg==', faceObj)
    .then(faceResponse => {
      let identifyObj = {    
        'personGroupId': 'customer',
        'faceIds': [faceResponse.data[0].faceId],
        'maxNumOfCandidatesReturned': 1,
        'confidenceThreshold': 0.5,
      };
      axios.post('https://hackthonfunction.azurewebsites.net/api/Identify?code=0a1w8ALPxSFPERpnQv4gCpz44IYV2duGQEXilq4/hymRWRePGnRdKQ==', identifyObj)
      .then(identifyResponse => {
        const personId = identifyResponse.data[0].candidates[0].personId;
        console.log(personId);
        let personObj = {
          personId,
        };
        if (personId) {
          axios.post('https://hackthonfunction.azurewebsites.net/api/PersonDetail?code=m8bz7hSfwXOyE3epgBnVEpDwFV5d8WwiQFoJ0Bhp14uRwKpEysbQWQ==', personObj)
          .then(personResponse => {
            const data = {
              'person': personResponse.data,
              'token': personResponse.data.personId,
              'name': personResponse.data.name,
              'detail': personResponse.data.userData,
              'image': response.data.Message,
            };
            dispatch({ type: types.WEBCAM_SUCCESS, data});
          })
          .catch(personError => {
            console.log(personError);
            const data = personError;
            dispatch({ type: types.WEBCAM_ERROR, data});
          }); 
        } else {
          const data = {
            error: 'Person Not found',
          };
          dispatch({ type: types.WEBCAM_ERROR, data});
        }
      })
      .catch(identifyError => {
        console.log(identifyError);
        const data = identifyError;
        dispatch({ type: types.WEBCAM_ERROR, data});
      }); 
    })
    .catch(faceError => {
      console.log(faceError);
      const data = faceError;
      dispatch({ type: types.WEBCAM_ERROR, data});
    });
  })
  .catch(error => {
    const data = error;
    dispatch({ type: types.WEBCAM_ERROR, data});
    console.log(error);
  }); */
};


export const showCam = () => dispatch => {
  dispatch({ type: types.SHOW_WEBCAM });
};

export const hideCam = () => dispatch => {
  dispatch({ type: types.HIDE_WEBCAM });
};



export const showBio = () => dispatch => {
  dispatch({ type: types.HIDE_TIMELINE });
};

export const showTimeline = () => dispatch => {
  dispatch({ type: types.SHOW_TIMELINE });
};

